#include "stdafx.h"
#include "LookCommand.h"


LookCommand::LookCommand()
{
	AddIdentifier("look");
	AddIdentifier("see");
	AddIdentifier("find");
	AddIdentifier("observe");
	//cout << _identifier.size() << endl;
}

void LookCommand::Execute(Player * currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	//cout << "testing look entered" << endl;
	if (enteredPhrase.size() == 1) {
		LookAround(currentPlayer, currentWorld);
	} else if (enteredPhrase.size() == 2 && enteredPhrase[1] != "around") {
		for each (Item* i in *(currentPlayer->GetLocation()->GetInventory()->_items)) {
			if (i->FirstId() == enteredPhrase[1]) {
				cout << i->FullDescription() << endl;
			}
		}
	} else if (enteredPhrase.size() == 2 && enteredPhrase[1] == "around") {
		LookAround(currentPlayer, currentWorld);
	} else if (enteredPhrase.size() == 3) {
		if (enteredPhrase[1] == "at") {
			for each (Item* i in *(currentPlayer->GetLocation()->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[2]) {
					cout << i->FullDescription() << endl;
				}
			}
		} else if (enteredPhrase[1] == "in") {
			if (enteredPhrase[2] == "location" || enteredPhrase[2] == "surroundings") {
				cout << "in the location around you is: " << endl;
				currentPlayer->GetLocation()->GetInventory()->ItemList();
			} else if (enteredPhrase[2] == "inventory" || enteredPhrase[2] == "player") {
				cout << "You have in your inventory" << endl;
				currentPlayer->GetInventory()->ItemList();
			}
		}
	} else if (enteredPhrase.size() == 4) {
		if (enteredPhrase[3] == "location" || enteredPhrase[3] == "surroundings") {
			for each (Item* i in *(currentPlayer->GetLocation()->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[1]) {
					cout << i->FullDescription() << endl;
				}
			}
		}
		if (enteredPhrase[3] == "inventory") {
			for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[1]) {
					cout << i->FullDescription() << endl;
				}
			}
		}
	} else if (enteredPhrase.size() == 5) {
		if (enteredPhrase[4] == "location" || enteredPhrase[4] == "surroundings") {
			for each (Item* i in *(currentPlayer->GetLocation()->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[2]) {
					cout << i->FullDescription() << endl;
				}
			}
		}
		if (enteredPhrase[4] == "inventory") {
			for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[2]) {
					cout << i->FullDescription() << endl;
				}
			}
		}
	} else {
		cout << "I'm not sure what you're looking for" << endl;
	}
}

void LookCommand::LookAround(Player * currentPlayer, GameWorld * currentWorld)
{
	cout << currentPlayer->GetLocation()->FullDescription() << endl;
	cout << "The items in this location are:" << endl;
	cout << currentPlayer->GetLocation()->GetInventory()->ItemList() << endl;
	cout << endl;
	cout << "You can move: " << endl;
	cout << currentPlayer->GetLocation()->PathList() << endl;
}

LookCommand::~LookCommand()
{
}
