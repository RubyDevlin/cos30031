#pragma once
#include "GameObject.h"

using namespace std;

class Item : public GameObject
{
public:
	Item();
	Item(vector<string> &ids, string name, string desc);
	~Item();
};

