#include "stdafx.h"
#include "Inventory.h"

Inventory::Inventory()
{
	this->_items = new std::vector<Item*>();
}

bool Inventory::HasItem(string id) const
{
	for (int i = 0; i < _items->size(); i++)
	{
		if (_items->at(i)->AreYou(id))
			return true;
	}

	return false;
}

string Inventory::ItemList() const
{
	string result = "";

	if (_items->size() <= 0)
		return "\tNothing";

	for (int i = 0; i < _items->size(); i++)
	{
		result += "\t" + _items->at(i)->ShortDescription();

		if (i < (_items->size() - 1))
			result += "\n";
	}

	return result;
}

void Inventory::Add(Item* item)
{
	_items->push_back(item);
}

Item* Inventory::Fetch(string itemId)
{
	for (int i = 0; i < _items->size(); i++)
	{
		if (_items->at(i)->AreYou(itemId))
			return _items->at(i);
	}

	return nullptr;
}

Item* Inventory::Remove(string itemId)
{
	for (int i = 0; i < _items->size(); i++)
	{
		if (_items->at(i)->AreYou(itemId))
		{
			Item* result = _items->at(i);
			_items->erase(_items->begin() + i);
			return result;
		}
	}

	return nullptr;
}

Inventory::~Inventory()
{
}