#include "stdafx.h"
#include "Player.h"

Player::Player()
{
	_inventory = new Inventory();
}

Player::Player(string name, string desc)
	: GameObject(vector<string> {"me", "inventory"}, name, desc)
{
	_inventory = new Inventory();
}

string Player::FullDescription() const
{
	return GameObject::FullDescription() + "\nYou are carrying:\n" + _inventory->ItemList();
}

string Player::Name() const
{
	return GameObject::Name();
}

Inventory* Player::GetInventory()
{
	return _inventory;
}

GameObject* Player::Locate(string id)
{
	if (AreYou(id))
		return this;

	return _inventory->Fetch(id);
}

void Player::SetLocation(Location* location)
{
	_location = location;
}

Location* Player::GetLocation()
{
	return _location;
}

Player::~Player()
{
	delete _inventory;
}
