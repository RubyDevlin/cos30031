#pragma once
#include "GameObject.h"
#include "Path.h"
#include "Inventory.h"
#include "IHaveInventory.h"

class Location :
	public GameObject, IHaveInventory 
{
private:
	Inventory* _inventory;
public:
	Location();
	Location(vector<string> &ids, string name, string desc);
	vector<Path*> _paths;
	string FullDescription() const;
	string PathList() const;
	string Name() const;
	Inventory* GetInventory();
	GameObject* Locate(string id);
	void AddPath(Path* p);
	~Location();
};

