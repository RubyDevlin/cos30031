#pragma once
#include "Location.h"
#include <vector>
#include <string>
using namespace std;

class GameWorld
{
private:
	
public:
	vector<Location*> _locations;
	GameWorld();
	void AddLocation(Location* location);
	~GameWorld();
};


