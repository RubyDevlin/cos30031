#pragma once
#include "State.h"
class Help :
	public State
{
public:
	Help();
	~Help();
	void Output();
	void Input(GameController* g, vector<string> s);
};

