#pragma once
#include "LookCommand.h"
#include "MoveCommand.h"

class CommandProcessing
{
private:
	LookCommand* _look;
	MoveCommand* _move;
public:
	CommandProcessing();
	void Run(Player* currentPlayer, GameWorld* currentWorld, vector<string> phrase);
	//LookCommand GetLook();
	//MoveCommand GetMove();
	~CommandProcessing();
};

