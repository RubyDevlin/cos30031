#pragma once
#include "Item.h"
#include"IHaveInventory.h"

class Bag :
	public Item, public IHaveInventory
{
private:
	Inventory* _inventory;
	bool _open;
public:
	Bag();
	Bag(vector<string> ids, string name, string desc, bool open);
	~Bag();
};

