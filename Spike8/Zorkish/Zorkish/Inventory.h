#pragma once
#include "GameObject.h"
#include "Item.h"

using namespace std;

class Inventory
{
private:
	
public:
	vector<Item*>* _items;
	Inventory();

	bool HasItem(string id) const;
	string ItemList() const;

	void Add(Item* item);
	Item* Fetch(string itemId);
	Item* Remove(string id);

	~Inventory();
};

