#pragma once
#include <iostream>
#include <string>
class State;

class GameController
{
public:
	GameController();
	~GameController();
	void ChangeState(State* s);
	void Output();
	void GameLoop();
	void Quit();
private:
	State* _state;
	bool quit;
};

