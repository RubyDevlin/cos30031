#pragma once
#include <iostream>
#include "GameController.h"
using namespace std;

class State
{
public:
	State();
	virtual ~State();
	virtual void Output() = 0;
	virtual void Input(GameController* g, string s) = 0;
};

