#include "stdafx.h"
#include "SelectAdventure.h"
#include "Gameplay.h"


SelectAdventure::SelectAdventure()
{
}


SelectAdventure::~SelectAdventure()
{
}

void SelectAdventure::Output()
{
	cout << "Zorkish::Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Choose your adventure:" << endl;
	cout << "1. Void World" << endl;
	//cout << "1.	Mountain World" << endl;
	//cout << "2. Water World" << endl;
	//cout << "3. Box	World" << endl;
	cout << "Select 1 - 3:" << endl;
}

void SelectAdventure::Input(GameController * g, string s)
{
	int selection = 0;
	selection = atoi(s.c_str());
	switch (selection)
	{
	case 1:
		g->ChangeState(new Gameplay());
		delete this;
		break;
	default:
		break;
	}
}
