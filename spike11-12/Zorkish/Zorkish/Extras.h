#pragma once

class Blackboard;

class Extras
{
private:
	static Blackboard* _blackboard;
	static void DeleteBlackboard();
public:
	static Blackboard* GetBlackboard();
	static void SetBlackboard(Blackboard* blackboard);
};

