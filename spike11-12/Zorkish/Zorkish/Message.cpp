#include "stdafx.h"
#include "Message.h"
#include "Player.h"
#include "Enemy.h"

Message::Message()
{
}

Message::Message(string target, string messageType):Message(target, messageType, nullptr)
{
}

Message::Message(string target, string messageType, GameObject* sender)
{
	_target = target;
	_messgeType = messageType;
	_sender = sender;
}

string Message::GetTarget() const
{
	return _target;
}

string Message::GetMessageType() const
{
	return _messgeType;
}

GameObject * Message::GetSender() const
{
	return _sender;
}

Message::~Message()
{
}
