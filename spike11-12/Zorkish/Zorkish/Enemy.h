#pragma once
#include "GameObject.h"
#include "AttackComponent.h"
#include "HealthComponent.h"
#include "IHaveInventory.h"
#include "Extras.h"

class Enemy :
	public GameObject, public IHaveInventory
{
private:
	bool _isAlive;
	Inventory* _inventory;
	AttackComponent* _attack;
	HealthComponent* _health;
public:
	Enemy();
	Enemy(string id, string name, string desc, int attack, int HP);

	string FullDescription();
	string Name() const;
	AttackComponent* GetAttackComponent();
	HealthComponent* GetHealthComponent();
	Inventory* GetInventory();
	bool IsAlive() const;
	GameObject* Locate(string id);
	string CheckMessage();
	string InterpretMessage(Message* message);

	~Enemy();
};

