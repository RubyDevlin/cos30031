#include "stdafx.h"
#include "Path.h"


Path::Path()
{
}

Path::Path(string direction, Location* connectedlocation, string desc) : GameObject(vector<string>{direction}, direction, desc)
{
	_connectedLocation = connectedlocation;
	_directionDescription = desc;
	_direction = direction;
}

string Path::DirectionDescription()
{
	return _directionDescription;
}

Location * Path::ConnectedLocation()
{
	return _connectedLocation;
}


Path::~Path()
{
}
