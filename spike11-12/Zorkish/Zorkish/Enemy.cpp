#include "stdafx.h"
#include "Enemy.h"
#include "Player.h"


Enemy::Enemy()
{
}

Enemy::Enemy(string id, string name, string desc, int attack, int HP) :GameObject(vector<string>{id},name,desc)
{
	_isAlive = true;
	_attack = new AttackComponent(attack);
	_health = new HealthComponent(HP);
}

string Enemy::FullDescription()
{
	return GameObject::ShortDescription() + 
		"\n" + GetHealthComponent()->GetHealth() + 
		"\n" + GetAttackComponent()->GetDamage();
}

string Enemy::Name() const
{
	return GameObject::Name();
}

AttackComponent * Enemy::GetAttackComponent()
{
	return _attack;
}

HealthComponent * Enemy::GetHealthComponent()
{
	return _health;
}

Inventory * Enemy::GetInventory()
{
	return _inventory;
}

bool Enemy::IsAlive() const
{
	return _isAlive;
}

GameObject * Enemy::Locate(string id)
{
	if (AreYou(id)) {
		return this;
	}
	if (_isAlive) {
		return nullptr;
	}
	GameObject* result;

	result = _inventory->Fetch(id);

	return result;
}

string Enemy::CheckMessage()
{
	Message* message = nullptr;
	message = Extras::GetBlackboard()->CheckShortMessage(this);
	if (message == nullptr) {
		return"";
	}
	return InterpretMessage(message);
}

string Enemy::InterpretMessage(Message * message)
{
	if (message->GetMessageType() == "attack") {
		AttackComponent* attack = new AttackComponent(1);

		Enemy* enemy = dynamic_cast<Enemy*>(message->GetSender());
		HealthComponent* health = this->GetHealthComponent();

		if (health == nullptr) {
			return Name() + " cannot be attacked";
		}
		
		attack->Attack(health);
		return "You attacked " + Name();
	}
	return GameObject::InterpretMessage(message);
}


Enemy::~Enemy()
{
}
