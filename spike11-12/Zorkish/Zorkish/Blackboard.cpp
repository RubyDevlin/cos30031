#include "stdafx.h"
#include "Blackboard.h"
#include "Enemy.h"
#include "Player.h"


Blackboard::Blackboard()
{
}

bool Blackboard::ShortRangeMessagesExist()
{
	return !_shortRangeMessages.empty();
}

Message * Blackboard::CheckMessage(vector<Message*>& messageHolder, GameObject * recipient)
{
	for each (Message* m in messageHolder) {
		string messageTarget = m->GetTarget();
		if (messageTarget == "all") {
			return m;
		}

		Enemy* enemy = dynamic_cast<Enemy*>(recipient);
		if (enemy != nullptr && messageTarget == "all enemies"){
			//cout << "Here's the sweet spot" << endl;
			return m;
		}

		if (recipient->AreYou(messageTarget)) {
			//cout << "Here is nice too" << endl;
			return m;
		}
	}
	return nullptr;
}

Message * Blackboard::CheckShortMessage(GameObject * recipient)
{
	return CheckMessage(_shortRangeMessages, recipient);
}

void Blackboard::ClearMessages()
{
	for each (Message* m in _shortRangeMessages) {
		delete m;
	}
	_shortRangeMessages.clear();
}

void Blackboard::CreateShortMessage(Message * message)
{
	_shortRangeMessages.push_back(message);
}


Blackboard::~Blackboard()
{
}
