#include "stdafx.h"
#include "AttackCommand.h"


AttackCommand::AttackCommand()
{
	AddIdentifier("attack");
	AddIdentifier("hit");
	AddIdentifier("strike");
	AddIdentifier("punch");
}

void AttackCommand::Execute(Player * currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	switch (enteredPhrase.size())
	{
	case(2):
	{
		if (enteredPhrase[1] == "world") {
			for each (Location* l in currentWorld->_locations) {
				for each (Enemy* e in l->_enemies) {
					Message* m = new Message(e->FirstId(), "attack");
					e->InterpretMessage(m);
				}
			}
		}
		if (enteredPhrase[1] == "all") {
			AttackAll(currentPlayer);
		}
		else {
			for each (Enemy* e in currentPlayer->GetLocation()->_enemies) {
				if (e->FirstId() == enteredPhrase[1]) {
					currentPlayer->GetAttackComponent()->Attack(e->GetHealthComponent());
				}
			}
		}
		break;
	}
	default:
		break;
	}
}

void AttackCommand::AttackAll(Player * currentPlayer)
{
	for each (Enemy* e in currentPlayer->GetLocation()->_enemies) {
		Message* m = new Message(e->FirstId(), "attack");
		Extras::GetBlackboard()->CreateShortMessage(m);
		cout << "attack all done" << endl;
	}
}


AttackCommand::~AttackCommand()
{
}
