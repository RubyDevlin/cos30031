#include "stdafx.h"
#include "Extras.h"

Blackboard* Extras::_blackboard = nullptr;

void Extras::DeleteBlackboard()
{
	if (_blackboard != nullptr) {
		delete _blackboard;
	}
}

Blackboard* Extras::GetBlackboard()
{
	return _blackboard;
}

void Extras::SetBlackboard(Blackboard* blackboard) {
	DeleteBlackboard();
	_blackboard = blackboard;
}