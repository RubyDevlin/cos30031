#pragma once
#include "GameObject.h"
#include "Path.h"
#include "Inventory.h"
#include "IHaveInventory.h"
#include "Enemy.h"

class Location :
	public GameObject, IHaveInventory 
{
private:
	Inventory* _inventory;
public:
	Location();
	Location(vector<string> &ids, string name, string desc);
	vector<Path*> _paths;
	vector<Enemy*> _enemies;
	string FullDescription() const;
	string PathList() const;
	string EnemyList() const;
	string Name() const;
	Inventory* GetInventory();
	GameObject* Locate(string id);

	string CheckMessage();
	string InterpretMessage(Message* message);

	void AddPath(Path* p);
	void AddEnemy(Enemy* e);
	~Location();
};

