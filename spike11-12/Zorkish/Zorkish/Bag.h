#pragma once
#include "Item.h"
#include"IHaveInventory.h"

class Bag :
	public Item, public IHaveInventory
{
private:
	Inventory* _inventory;
	bool _open;
	
public:
	Bag();
	bool _IHaveInventory;
	Bag(vector<string> ids, string name, string desc, bool open);
	string FullDescription();
	bool IsOpen();
	string Name() const;
	string Close();
	string Open();
	Inventory* GetInventory();
	GameObject* Locate(string id);
	~Bag();
};

