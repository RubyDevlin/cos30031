#pragma once
#include "Message.h"

class Blackboard
{
private:
	vector<Message*> _shortRangeMessages;
public:
	Blackboard();
	bool ShortRangeMessagesExist();

	Message* CheckMessage(vector<Message*>& messageHolder, GameObject* recipient);
	Message* CheckShortMessage(GameObject* recipient);
	void ClearMessages();
	void CreateShortMessage(Message* message);

	~Blackboard();
};

