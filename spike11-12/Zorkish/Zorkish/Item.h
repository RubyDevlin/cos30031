#pragma once
#include "GameObject.h"

using namespace std;

class Item : public GameObject
{
private:
	
public:
	Item();
	bool _IHaveInventory;
	Item(vector<string> &ids, string name, string desc);
	~Item();
};

