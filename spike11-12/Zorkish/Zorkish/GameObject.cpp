#include "stdafx.h"
#include "GameObject.h"
#include "Extras.h"

GameObject::GameObject()
{
}

GameObject::GameObject(vector<string> &ids, string name, string desc)
	: IdentifiableObject(ids)
{
	_name = name;
	_description = desc;
}

string GameObject::FullDescription() const
{
	return _description;
}

string GameObject::Name() const
{
	return _name;
}

string GameObject::ShortDescription() //const
{
	return _name + " (" + FirstId() + ")";
}

string GameObject::InterpretMessage(Message * message)
{
	//cout << "OH NO NO NON" << endl;
	if (message->GetMessageType() == "attack") {
		return "The " + Name() + " cannot be attacked";
	}
	if (message->GetMessageType() == "moveto") {
		return "The " + Name() + " is not a location";
	}
	return"";
}

string GameObject::CheckMessage()
{
	Message* message = nullptr;
	message = Extras::GetBlackboard()->CheckShortMessage(this);
	if (message == nullptr) {
		return"";
	}
	return InterpretMessage(message);
}

GameObject::~GameObject()
{
}
