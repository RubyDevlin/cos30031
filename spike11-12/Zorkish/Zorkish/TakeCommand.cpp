#include "stdafx.h"
#include "TakeCommand.h"
#include "Bag.h"


TakeCommand::TakeCommand()
{
	AddIdentifier("take");
	AddIdentifier("grab");
	AddIdentifier("remove");
	AddIdentifier("acquire");
}

void TakeCommand::Execute(Player* currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	if (enteredPhrase.size() == 2) {
		for each (Item* i in *(currentPlayer->GetLocation()->GetInventory()->_items)) {
			currentPlayer->GetInventory()->Add(i);
			cout << "You have picked up " << i->Name() << endl;
			currentPlayer->GetLocation()->GetInventory()->Remove(i->FirstId());
		}
	}
	if (enteredPhrase.size() == 4) {
		for each (Bag* b in *(currentPlayer->GetInventory()->_items)) {
			if (!b->IsOpen() && b->FirstId() == enteredPhrase[3]) {
				cout << b->Name() << " is closed" << endl;
			}
			if (b->IsOpen() && b->FirstId() == enteredPhrase[3]) {
				for each (Item* i in *(b->GetInventory()->_items)) {
					if (i->FirstId() == enteredPhrase[1]) {
						currentPlayer->GetInventory()->Add(i);
						b->GetInventory()->Remove(i->FirstId());
					}
				}
			}
		}
	}
}


TakeCommand::~TakeCommand()
{
}
