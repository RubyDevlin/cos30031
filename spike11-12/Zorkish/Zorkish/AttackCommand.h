#pragma once
#include "Command.h"
class AttackCommand :
	public Command
{
public:
	AttackCommand();
	void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase);
	void AttackAll(Player* currentPlayer);
	~AttackCommand();
};

