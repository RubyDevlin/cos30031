#pragma once
#include "State.h"
class SelectAdventure :
	public State
{
public:
	SelectAdventure();
	~SelectAdventure();
	void Output();
	void Input(GameController* g, vector<string> s);
};

