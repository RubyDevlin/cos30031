#include "stdafx.h"
#include "AttackComponent.h"


AttackComponent::AttackComponent()
{
}

AttackComponent::AttackComponent(int damage)
{
	_damage = damage;
}

string AttackComponent::GetDamage() const
{
	return to_string(_damage) + " damage";
}

void AttackComponent::Attack(HealthComponent * target)
{
	double randomiseDamage = (rand() % 101 - 50) / 500.0;
	_damage += (int)round(randomiseDamage*_damage);

	target->ReduceHealth(_damage);
}


AttackComponent::~AttackComponent()
{
}
