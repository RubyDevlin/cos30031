// Zorkish.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GameController.h"
#include "Player.h"
#include "GameWorld.h"

using namespace std;

int main()
{	
	GameController* myGame = new GameController();

	myGame->GameLoop();

    return 0;
}

