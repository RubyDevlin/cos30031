#pragma once
#include "HealthComponent.h"

class AttackComponent
{
private:
	int _damage;
public:
	AttackComponent();
	AttackComponent(int damage);

	string GetDamage() const;
	void Attack(HealthComponent* target);

	~AttackComponent();
};

