#pragma once
#include "Command.h"
class PutDownCommand :
	public Command
{
public:
	PutDownCommand();
	void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase);
	~PutDownCommand();
};

