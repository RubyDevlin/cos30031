#pragma once
#include<string>
using namespace std;

class HealthComponent
{
private:
	int _maxHP;
	int _currentHP;
public:
	HealthComponent();
	HealthComponent(int maxHP);
	HealthComponent(int currentHP, int maxHP);

	string GetHealth();
	void ReduceHealth(int damage);

	~HealthComponent();
};

