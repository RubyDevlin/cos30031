#pragma once
#include "State.h"
#include "Player.h"
#include "Location.h"
#include "GameWorld.h"
#include "CommandProcessing.h"

class Gameplay :
	public State
{
private:
	bool _setupGame;
public:
	GameWorld MountainWorld;
	CommandProcessing* _commandProcessor;
	Location* _testLocation;
	Player* _currentPlayer;
	Gameplay();
	~Gameplay();
	void SetupGame();
	void Output();
	void Input(GameController* g, vector<string> s);
	//void MovementInput(string s);
};

