#pragma once
#include "Command.h"
class LookCommand :
	public Command
{
public:
	LookCommand();
	void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase);
	void LookAround(Player* currentPlayer, GameWorld* currentWorld);
	~LookCommand();
};

