#pragma once
#include "Command.h"
class TakeCommand :
	public Command
{
public:
	TakeCommand();
	void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase);
	~TakeCommand();
};

