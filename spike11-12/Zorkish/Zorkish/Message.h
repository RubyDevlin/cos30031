#pragma once
#include <string>
#include <vector>

using namespace std;
class GameObject;

class Message
{
	string _target, _messgeType;
	GameObject* _sender;
public:
	Message();
	Message(string target, string messageType);
	Message(string target, string messageType, GameObject* sender);

	string GetTarget() const;
	string GetMessageType() const;
	GameObject* GetSender() const;

	void SetSender(GameObject* sender);

	~Message();
};

