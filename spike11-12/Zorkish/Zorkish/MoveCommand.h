#pragma once
#include "Command.h"
class MoveCommand :
	public Command
{
public:
	MoveCommand();
	void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase);
	~MoveCommand();
};

