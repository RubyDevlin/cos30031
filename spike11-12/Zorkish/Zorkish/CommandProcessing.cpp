#include "stdafx.h"
#include "CommandProcessing.h"


CommandProcessing::CommandProcessing()
{
	_look = new LookCommand();
	_move = new MoveCommand();
	_take = new TakeCommand();
	_openclose = new OpenCloseCommand();
	_putdown = new PutDownCommand();
	_attack = new AttackCommand();
}

void CommandProcessing::Run(Player* currentPlayer, GameWorld* currentWorld, vector<string> phrase)
{
	//cout << _look->FirstId() << endl;
	if (!phrase.empty()) {
		if (_look->AreYou(phrase[0]))
		{
			_look->Execute(currentPlayer, currentWorld, phrase);
		}
		if (_move->AreYou(phrase[0])) {
			_move->Execute(currentPlayer, currentWorld, phrase);
		}
		if (_take->AreYou(phrase[0]))
		{
			_take->Execute(currentPlayer, currentWorld, phrase);
		}
		if (_openclose->AreYou(phrase[0])) {
			_openclose->Execute(currentPlayer, currentWorld, phrase);
		}
		if (_putdown->AreYou(phrase[0])) {
			_putdown->Execute(currentPlayer, currentWorld, phrase);
		}
		if (_attack->AreYou(phrase[0])) {
			_attack->Execute(currentPlayer, currentWorld, phrase);
		}
	}
	else {
		cout << "Please enter a valid command" << endl;
	}
}

//LookCommand CommandProcessing::GetLook()
//{
//	return _look;
//}

//MoveCommand CommandProcessing::GetMove()
//{
//	return _move;
//}


CommandProcessing::~CommandProcessing()
{
}
