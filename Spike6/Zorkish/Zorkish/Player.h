#pragma once
#include "GameObject.h"
#include "Inventory.h"
#include "IHaveInventory.h"

using namespace std;

class Player : public GameObject, public IHaveInventory
{
private:
	Inventory* _inventory;
public:
	Player();
	Player(string name, string desc);

	string FullDescription() const;
	string Name() const;

	Inventory* GetInventory();
	GameObject* Locate(string id);

	~Player();
};

