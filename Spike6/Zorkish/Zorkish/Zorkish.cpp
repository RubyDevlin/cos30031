// Zorkish.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GameController.h"
#include "Player.h"

using namespace std;

int main()
{
	Player* player = new Player("You", "A Hero");

	player->GetInventory()->Add(new Item(vector<string> {"spoon", "cutlery"}, "Spoon", "A Silver Spoon"));
	player->GetInventory()->Add(new Item(vector<string> {"fork", "cutlery"}, "Fork", "A Sharp Fork"));

	cout << player->FullDescription() << endl;

	player->GetInventory()->Remove("fork");

	cout << player->FullDescription() << endl;

	string _test;
	cin >> _test;
	//GameController* myGame = new GameController();

	//myGame->GameLoop();

    return 0;
}

