#pragma once
#include "State.h"
class ViewHallOfFame :
	public State
{
public:
	ViewHallOfFame();
	~ViewHallOfFame();
	void Output();
	void Input(GameController* g, string s);
};

