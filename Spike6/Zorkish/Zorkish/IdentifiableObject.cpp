#include "stdafx.h"
#include "IdentifiableObject.h"

IdentifiableObject::IdentifiableObject()
{
}

IdentifiableObject::IdentifiableObject(vector<string> &arr)
{
	for (int i = 0; i < arr.size(); i++)
	{
		transform(arr[i].begin(), arr[i].end(), arr[i].begin(), ::tolower);
		_identifier.push_back(arr[i]);
	}
}

bool IdentifiableObject::AreYou(string id) const
{
	transform(id.begin(), id.end(), id.begin(), ::tolower);

	for (int i = 0; i < _identifier.size(); i++)
	{
		if (_identifier[i] == id)
			return true;
	}

	return false;
}

string IdentifiableObject::FirstId() const
{
	if (_identifier.size() > 0)
	{
		return _identifier[0];
	} else {
		return "";
	}		
}

void IdentifiableObject::AddIdentifier(string id)
{
	transform(id.begin(), id.end(), id.begin(), ::tolower);

	_identifier.push_back(id);
}

IdentifiableObject::~IdentifiableObject()
{
}
