#include "stdafx.h"
#include "NewHighScore.h"


NewHighScore::NewHighScore()
{
}


NewHighScore::~NewHighScore()
{
}

void NewHighScore::Output()
{
	cout << "Zorkish::New High Score" << endl;
	cout << "-------------------------------------------------------- " << endl;
	cout << "Congratulations!" << endl;
	cout << "You have made it to the Zorkish Hall Of Fame" << endl;
	cout << "Adventure :[the adventure world played]" << endl;
	cout << "Score :[the players score]" << endl;
	cout << "Moves :[number of moves player made]" << endl;
	cout << "Please type your name and press enter: " << endl;				
}
