#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Zorkish/Item.h"
#include "../Zorkish/Inventory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Spike6Test
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestItemsAdded)
		{
			Inventory* testInventory = new Inventory();
			Item* testItem1 = new Item(vector<string>{"testitem1"}, "Test Item 1", "The first test item");
			testInventory->Add(testItem1);
			Assert::IsTrue(testInventory->HasItem(testItem1->FirstId()));
		}
	};

	TEST_CLASS(UnitTest2)
	{
		TEST_METHOD(TestItemsRemoved)
		{
			Inventory* testInventory = new Inventory();
			Item* testItem1 = new Item(vector<string>{"testitem1"}, "Test Item 1", "The first test item");
			testInventory->Add(testItem1);
			testInventory->Remove(testItem1->FirstId());
			Assert::IsFalse(testInventory->HasItem(testItem1->FirstId()));
		}
	};

	TEST_CLASS(UnitTest3)
	{
		TEST_METHOD(TestFetchItem)
		{
			Inventory* testInventory = new Inventory();
			Item* testItem1 = new Item(vector<string>{"testitem1"}, "Test Item 1", "The first test item");
			testInventory->Add(testItem1);
			Assert::IsTrue(testInventory->Fetch(testItem1->FirstId()) == testItem1);
		}
	};
}