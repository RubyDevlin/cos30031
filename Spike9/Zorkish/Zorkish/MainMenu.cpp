#include "stdafx.h"
#include "MainMenu.h"
#include "SelectAdventure.h"
#include "ViewHallOfFame.h"
#include "Help.h"
#include "About.h"

MainMenu::MainMenu()
{
}


MainMenu::~MainMenu()
{
}

void MainMenu::Output()
{
	cout << "Zorkish :: Main Menu" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Welcome to Zorkish Adventures" << endl;
	cout << "1. Select Adventure and Play" << endl;
	cout << "2. Hall Of Fame" << endl;
	cout << "3. Help" << endl;
	cout << "4. About" << endl;
	cout << "5. Quit" << endl;
	cout << "Select 1 - 5:" << endl;
	cout << ">" << endl;
 }

void MainMenu::Input(GameController* g, vector<string> s)
{
	int selection = 0;
	if (!s.empty())
		selection = atoi(s[0].c_str());
	switch (selection)
	{
	case 1:
		g->ChangeState(new SelectAdventure());
		delete this;
		break;
	case 2:
		g->ChangeState(new ViewHallOfFame());
		delete this;
		break;
	case 3:
		g->ChangeState(new Help());
		delete this;
		break;
	case 4:
		g->ChangeState(new About());
		delete this;
		break;
	case 5:
		g->Quit();
		break;
	default:
		break;
	}
}