#include "stdafx.h"
#include "SelectAdventure.h"
#include "Gameplay.h"


SelectAdventure::SelectAdventure()
{
}


SelectAdventure::~SelectAdventure()
{
}

void SelectAdventure::Output()
{
	cout << "Zorkish::Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Choose your adventure:" << endl;
	cout << "1.	Mountain World" << endl;
	//cout << "2. Water World" << endl;
	//cout << "3. Box	World" << endl;
	cout << "Select 1 - 3:" << endl;
}

void SelectAdventure::Input(GameController * g, vector<string> s)
{
	if (!s.empty() && s[0] == "1") {
		g->ChangeState(new Gameplay());
		delete this;
	}
}
