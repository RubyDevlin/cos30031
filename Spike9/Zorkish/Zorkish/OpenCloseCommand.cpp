#include "stdafx.h"
#include "OpenCloseCommand.h"


OpenCloseCommand::OpenCloseCommand()
{
	AddIdentifier("open");
	AddIdentifier("close");
}

void OpenCloseCommand::Execute(Player * currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	if (enteredPhrase[0] == "open") {
		for each (Bag* b in *(currentPlayer->GetInventory()->_items)) {
			if (b->_IHaveInventory == true) {
				cout << b->Open() << endl;
			}
		}
	}
	if (enteredPhrase[0] == "close") {
		for each (Bag* b in *(currentPlayer->GetInventory()->_items)) {
			if (b->_IHaveInventory = true) {
				cout << b->Close() << endl;
			}
		}
	}
}


OpenCloseCommand::~OpenCloseCommand()
{
}
