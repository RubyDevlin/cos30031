#include "stdafx.h"
#include "MoveCommand.h"


MoveCommand::MoveCommand()
{
	AddIdentifier("go");
	AddIdentifier("move");
	AddIdentifier("walk");
	AddIdentifier("travel");
}

void MoveCommand::Execute(Player * currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	cout << "testing move entered" << endl;
	int i = 0;

	if (enteredPhrase.size() == 1)
	{
		i = 0;
	}
	if (enteredPhrase.size() == 2) {
		i = 1;
	}

	if (enteredPhrase[i] == "north" || enteredPhrase[i] == "n")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "north") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	else if (enteredPhrase[i] == "west" || enteredPhrase[i] == "w")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "west") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	else if (enteredPhrase[i] == "east" || enteredPhrase[i] == "e")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "east") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	else if (enteredPhrase[i] == "south" || enteredPhrase[i] == "s")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "south") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	else if (enteredPhrase[i] == "northeast" || enteredPhrase[i] == "ne")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "northeast") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}

	else if (enteredPhrase[i] == "northwest" || enteredPhrase[i] == "nw")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "northwest") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}

	else if (enteredPhrase[i] == "southwest" || enteredPhrase[i] == "sw")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "southwest") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	else if (enteredPhrase[i] == "southwast" || enteredPhrase[i] == "se")
	{
		for each(Path* p in currentPlayer->GetLocation()->_paths) {
			if (p->FirstId() == "southeast") {
				currentPlayer->SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	else {
		cout << "I'm not sure how you want to move" << endl;
	}
}


MoveCommand::~MoveCommand()
{
}
