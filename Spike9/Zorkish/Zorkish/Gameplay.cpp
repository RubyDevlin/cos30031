#include "stdafx.h"
#include "Gameplay.h"
#include "MainMenu.h"
#include "ViewHallOfFame.h"
#include "Player.h"
#include <fstream>
#include <sstream>
#include "Bag.h"
//#include <istream>
#include <iterator>
using namespace std;



Gameplay::Gameplay()
{
	_setupGame = false;
	_currentPlayer = new Player();
	_commandProcessor = new CommandProcessing();
	
	//_currentPlayer.SetLocation(_testLocation);
}


Gameplay::~Gameplay()
{
}

void Gameplay::SetupGame()
{
	GameWorld MountainWorld = GameWorld();
	
	string line;
	int _creatingLocation = 0;
	string _locationId, _locationName, _locationDescription;
	int _creatingPath = 0;
	string _pathDirection, _pathDescription, _pathStartingLocation;
	int _creatingItem = 0;
	string _itemId, _itemName, _itemDescription, _itemLocation;
	Location* _pathToId = new Location(vector<string>{"default"}, "default", "default");
	Location* _pathFromId  = new Location(vector<string>{"default2"}, "default2", "default2");
	int _creatingBag = 0;
	string _bagId, _bagName, _bagDescription, _bagLocation;
	//Location* _pathLocation = new Location();

	ifstream AdventureFile("MountainWorld.txt");
	if (AdventureFile.is_open()) {
		while (getline(AdventureFile, line)) {
			if (_creatingLocation == 3) {
				_locationDescription = line;
				_creatingLocation = 0;
				//cout << "yo!";
				MountainWorld.AddLocation(new Location(vector<string>{_locationId}, _locationName, _locationDescription));
				//cout << "created location";
			}
			if (_creatingLocation == 2) {
				_locationName = line;
				_creatingLocation++;
				//cout << "case";
			}
			if (_creatingLocation == 1) {
				_locationId = line;
				_creatingLocation++;
				//cout << "case";
			}
			if (line == "Location") {
				//cout << "case";
				_creatingLocation++;
			}

			if (_creatingPath == 4)
			{
				_pathDescription = line;
				_creatingPath = 0;
				Path* path = new Path(_pathDirection, _pathToId, _pathDescription);

				for each (Location* l in MountainWorld._locations)
				{
					//cout << "path4";
					//cout << l->FirstId() << " and " << _pathStartingLocation << endl;
					if (l->FirstId() == _pathStartingLocation) {
						//cout << "Hello Ruby, you're the best" << endl;
						l->AddPath(path);
						//cout << l->PathList() << endl;
						
						//cout << "path4";
					}
				}
				
			}
			if (_creatingPath == 3) {
				for each (Location* l in MountainWorld._locations) {
					if (l->FirstId() == line) {
						_pathToId = l;
						//cout << "path3";
					}
				}
				_creatingPath++;
				
			}

			if (_creatingPath == 2) {
				_pathDirection = line;
				_creatingPath++;
				//cout << "path2";
			}

			if (_creatingPath == 1) {
				_pathStartingLocation = line;
				for each (Location* l in MountainWorld._locations) {
					if (l->FirstId() == line) {
						_pathFromId = l;
					}
				}
				//cout << "path1";
				_creatingPath++;
			}
			if (line == "ConnectionPath") {
				_creatingPath++;
				//cout << "path";
			}

			if (_creatingItem == 5) {
				if (_itemLocation == "Player")
				{
					_currentPlayer->GetInventory()->Add(new Item(vector<string>{_itemId}, _itemName, _itemDescription));
					//cout << _currentPlayer.FullDescription() << endl;

				} else {
					for each (Location* l in MountainWorld._locations)
					{
						if (l->FirstId() == _itemLocation) {
							l->GetInventory()->Add(new Item(vector<string>{_itemId}, _itemName, _itemDescription));
						}
					}
					for each (Bag* b in *(_currentPlayer->GetInventory()->_items)) 
					{
						if (b->FirstId() == _itemLocation) {
							b->GetInventory()->Add(new Item(vector<string>{_itemId}, _itemName, _itemDescription));
						}
					}
				}
				_creatingItem = 0;
			}

			if (_creatingItem == 4) {
				//cout << "item4" << endl;
				_itemLocation = line;
				_creatingItem++;
			}
			if (_creatingItem == 3) {
				//cout << "item3" << endl;
				_itemDescription = line;
				_creatingItem++;
			}
			if (_creatingItem == 2) {
				//cout << "item2" << endl;
				_itemName = line;
				_creatingItem++;
			}
			if (_creatingItem == 1) {
				//cout << "item1" << endl;
				_itemId = line;
				_creatingItem++;
			}
			if (line == "Item") {
				_creatingItem++;
			}

			if (_creatingBag == 5) {
				if (_bagLocation == "Player") {
					_currentPlayer->GetInventory()->Add(new Bag(vector<string>{_bagId}, _bagName, _bagDescription, false));
				}
				else {
					for each (Location* l in MountainWorld._locations) {
						if (l->FirstId() == _bagLocation) {
							l->GetInventory()->Add(new Bag(vector<string>{_bagId}, _bagName, _bagDescription, false));
						}
					}
				}
				//cout << _currentPlayer->GetInventory()->ItemList();
				_creatingBag = 0;
			}
			if (_creatingBag == 4) {
				_bagLocation = line;
				_creatingBag++;
			}
			if (_creatingBag == 3) {
				_bagDescription = line;
				_creatingBag++;
			}
			if (_creatingBag == 2) {
				_bagName = line;
				_creatingBag++;
			}
			if (_creatingBag == 1) {
				_bagId = line;
				_creatingBag++;
			}
			if (line == "Bag") {
				_creatingBag++;
			}

			//cout << line << endl;
			//system("pause");
			
			_setupGame = true;
		}
	}
	//cout << to_string(MountainWorld._locations.size()) << endl;
	_currentPlayer->SetLocation(MountainWorld._locations[0]);
	//for each(Location* l in MountainWorld._locations) {
	//	cout << l->Name() << endl;
	//	cout << l->PathList() << endl;
	//}
}
void Gameplay::Output()
{
	if (_setupGame == false) {
		SetupGame();
		cout << "Welcome to Zorkish:Mountain World. This world is simple and pointless, like all text adventure worlds." << endl;
	}
	cout << _currentPlayer->GetLocation()->FullDescription() << endl;
}

void Gameplay::Input(GameController* g, vector<string> s)
{
	_commandProcessor->Run(_currentPlayer, &MountainWorld, s);

	s.clear();
}
