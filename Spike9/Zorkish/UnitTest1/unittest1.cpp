#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Zorkish/Bag.h"
#include "../Zorkish/Gameplay.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestItemAddedToBag)
		{
			Bag* testBag = new Bag(vector<string>{"testBag"}, "Test Bag", "A bag for testing", true);
			Item* testItem = new Item(vector<string>{"testItem"}, "Test Item", "An item for testing");
			testBag->GetInventory()->Add(testItem);
			Assert::IsTrue(testBag->GetInventory()->HasItem(testItem->FirstId()));
		}
	};

	TEST_CLASS(UnitTest2)
	{
	public:

		TEST_METHOD(TestItemRemovedFromBag)
		{
			Bag* testBag = new Bag(vector<string>{"testBag"}, "Test Bag", "A bag for testing", true);
			Item* testItem = new Item(vector<string>{"testItem"}, "Test Item", "An item for testing");
			testBag->GetInventory()->Add(testItem);
			testBag->GetInventory()->Remove(testItem->FirstId());
			Assert::IsFalse(testBag->GetInventory()->HasItem(testItem->FirstId()));
		}

	};

	TEST_CLASS(UnitTest3)
	{
	public:

		TEST_METHOD(TestItemFetchedFromBag)
		{
			Bag* testBag = new Bag(vector<string>{"testBag"}, "Test Bag", "A bag for testing", true);
			Item* testItem = new Item(vector<string>{"testItem"}, "Test Item", "An item for testing");
			testBag->GetInventory()->Add(testItem);
			Assert::IsTrue(testBag->GetInventory()->Fetch(testItem->FirstId()) == testItem);
		}

	};
}