#pragma once
#include "Command.h"
class OpenCloseCommand :
	public Command
{
public:
	OpenCloseCommand();
	void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase);
	~OpenCloseCommand();
};

