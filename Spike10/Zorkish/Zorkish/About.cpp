#include "stdafx.h"
#include "About.h"
#include "MainMenu.h"


About::About()
{
}


About::~About()
{
}

void About::Output()
{
	cout << "Zorkish::About" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Written by: Ruby Devlin" << endl;
	cout << "Press 1 to return to the Main Menu" << endl;	
}

void About::Input(GameController* g, vector<string> s)
{
	if (!s.empty() && s[0] == "1" ) {
		g->ChangeState(new MainMenu);
		delete this;
	}
}
