#pragma once
#include "IdentifiableObject.h"
#include "Player.h"
#include "GameWorld.h"
#include "Bag.h"

class Command :
	public IdentifiableObject
{
public:
	Command();
	virtual void Execute(Player* currentPlayer, GameWorld* currentWorld, vector<string> enteredPhrase) = 0;
	~Command();
};

