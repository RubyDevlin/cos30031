#pragma once
#include "LookCommand.h"
#include "MoveCommand.h"
#include "TakeCommand.h"
#include "OpenCloseCommand.h"
#include "PutDownCommand.h"
#include "AttackCommand.h"

class CommandProcessing
{
private:
	LookCommand* _look;
	MoveCommand* _move;
	TakeCommand* _take;
	OpenCloseCommand* _openclose;
	PutDownCommand* _putdown;
	AttackCommand* _attack;
public:
	CommandProcessing();
	void Run(Player* currentPlayer, GameWorld* currentWorld, vector<string> phrase);
	//LookCommand GetLook();
	//MoveCommand GetMove();
	~CommandProcessing();
};

