#include "stdafx.h"
#include "Location.h"


Location::Location()
{
}

Location::Location(vector<string> &ids, string name, string desc)
	: GameObject(ids, name, desc)
{
	_inventory = new Inventory;
}

string Location::FullDescription() const
{
	string result = "You are in " + Name() +
		"\n" + GameObject::FullDescription();

	if (!(_paths.size() <= 0))
	{
		result += "\n" + PathList();
	}

	if (!(_enemies.size() <= 0)) {
		result += "\n" + EnemyList();
	}

	return result;
}

string Location::PathList() const
{
	string result = "You can go ";

	for (int i = 0; i < (int)_paths.size(); i++)
	{
		result += _paths[i]->Name() + " ";
	}
	return result;
}

string Location::EnemyList() const 
{
	string result = "There ";

	if (_enemies.size() == 0) {
		result += "are no enemies";
	}
	else if (_enemies.size() == 1) {
		result += "is one enemy";
	}
	else {
		result += "are " + to_string((int)_enemies.size()) + " enemies";
	}

	result += " in your current location";

	for each(Enemy* e in _enemies)
	{
		result += "\n\t" + e->FullDescription();
		if (!e->IsAlive()) {
			result += " is dead.";
		}
	}
	return result;
}

string Location::Name() const
{
	return GameObject::Name();
}

Inventory* Location::GetInventory()
{
	return _inventory;
}

GameObject* Location::Locate(string id)
{
	if (AreYou(id))
		return this;

	return _inventory->Fetch(id);
}

void Location::AddPath(Path* path)
{
	_paths.push_back(path);
}

void Location::AddEnemy(Enemy * e)
{
	_enemies.push_back(e);
}

Location::~Location()
{
}
