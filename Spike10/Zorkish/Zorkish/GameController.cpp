#include "stdafx.h"
#include "GameController.h"
#include "State.h"
#include "MainMenu.h"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;


GameController::GameController()
{
	_state = new MainMenu();
	quit = false;
}

GameController::~GameController()
{
	delete _state;
}

void GameController::ChangeState(State * s)
{
	_state = s;
}

void GameController::Output()
{
	_state->Output();
}

void GameController::GameLoop()
{
	while (!quit) 
	{
		vector<string> userInput;
		string input = "";
		Output();
		cout << "What do you do?" << endl;
		getline(cin, input);

		if (!input.empty()) {
			stringstream ss;
			ss.str(input);
			string item;

			while (getline(ss, item, ' ')) {
				userInput.push_back(item);
			}
		}

		//cin >> userInput;
		if (!userInput.empty() && userInput.at(0) == "quit")
		{
			Quit();
		} else {
			_state->Input(this, userInput);
		}
	}
}

void GameController::Quit()
{
	quit = true;
}
