#include "stdafx.h"
#include "AttackCommand.h"


AttackCommand::AttackCommand()
{
	AddIdentifier("attack");
	AddIdentifier("hit");
	AddIdentifier("strike");
	AddIdentifier("punch");
}

void AttackCommand::Execute(Player * currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	switch (enteredPhrase.size())
	{
	case(2):
	{
		for each (Enemy* e in currentPlayer->GetLocation()->_enemies) {
			if (e->FirstId() == enteredPhrase[1]) {
				currentPlayer->GetAttackComponent()->Attack(e->GetHealthComponent());
			}
		}
		break;
	}
	default:
		break;
	}
}


AttackCommand::~AttackCommand()
{
}
