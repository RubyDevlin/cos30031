#include "stdafx.h"
#include "PutDownCommand.h"


PutDownCommand::PutDownCommand()
{
	AddIdentifier("drop");
	AddIdentifier("put");
}

void PutDownCommand::Execute(Player * currentPlayer, GameWorld * currentWorld, vector<string> enteredPhrase)
{
	if (enteredPhrase.size() == 2) {
		for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
			if (i->FirstId() == enteredPhrase[1]) {
				currentPlayer->GetLocation()->GetInventory()->Add(i);
				cout << "You have dropped: " << i->Name() << endl;
				currentPlayer->GetInventory()->Remove(i->FirstId());
			}
		}
	}
	else if (enteredPhrase.size()==3) {
		if (enteredPhrase[1] == "down") {
			for each(Item* i in *(currentPlayer->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[2]) {
					currentPlayer->GetLocation()->GetInventory()->Add(i);
					currentPlayer->GetInventory()->Remove(i->FirstId());
					return;
				}
			}
		}
		if (enteredPhrase[2] == "down") {
			for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
				if (i->FirstId() == enteredPhrase[1]) {
					currentPlayer->GetLocation()->GetInventory()->Add(i);
					currentPlayer->GetInventory()->Remove(i->FirstId());
					return;
				}
			}
		}
	}
	else if (enteredPhrase.size() == 4) {
		if (enteredPhrase[2] == "into" || enteredPhrase[2] == "in") {
			for each (Bag* b in *(currentPlayer->GetInventory()->_items)) {
				if (!b->IsOpen() && b->FirstId() == enteredPhrase[3]) {
					cout << b->Name() << " is closed" << endl;
					return;
				}
				if (b->IsOpen() && b->FirstId() == enteredPhrase[3]) {
					for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
						b->GetInventory()->Add(i);
						currentPlayer->GetInventory()->Remove(i->FirstId());
						cout << "You have put " << i->Name() << " into your " << b->Name() << endl;
						return;
					}					
				}
			}
		}
		if (enteredPhrase[2]=="from") {
			for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
				if (i->Name() == enteredPhrase[1]) {
					currentPlayer->GetLocation()->GetInventory()->Add(i);
					currentPlayer->GetInventory()->Remove(i->FirstId());
					cout << "You have dropped: " << i->Name() << endl;
					return;
				}
			}
		}
		else {
			for each (Bag* b in *(currentPlayer->GetInventory()->_items)) {
				if (!b->IsOpen() && b->FirstId() == enteredPhrase[3]) {
					cout << b->Name() << " is closed" << endl;
				}
				if (b->IsOpen() && b->FirstId() == enteredPhrase[3]) {
					for each (Item* i in *(currentPlayer->GetInventory()->_items)) {
						if (i->FirstId() == enteredPhrase[1]) {
							b->GetInventory()->Remove(i->FirstId());
							currentPlayer->GetLocation()->GetInventory()->Add(i);
							cout << "You have dropped " << i->Name() << " from your " << b->Name() << endl;
						}
					}

				}
			}
		}
	}
}


PutDownCommand::~PutDownCommand()
{
}
