#include "stdafx.h"
#include "Enemy.h"


Enemy::Enemy()
{
}

Enemy::Enemy(string name, string desc, int attack, int HP) :GameObject(vector<string>{"enemy"},name,desc)
{
	_isAlive = true;
	_attack = new AttackComponent(attack);
	_health = new HealthComponent(HP);
}

string Enemy::FullDescription()
{
	return GameObject::ShortDescription() + 
		"\n" + GetHealthComponent()->GetHealth() + 
		"\n" + GetAttackComponent()->GetDamage();
}

string Enemy::Name() const
{
	return GameObject::Name();
}

AttackComponent * Enemy::GetAttackComponent()
{
	return _attack;
}

HealthComponent * Enemy::GetHealthComponent()
{
	return _health;
}

Inventory * Enemy::GetInventory()
{
	return _inventory;
}

bool Enemy::IsAlive() const
{
	return _isAlive;
}

GameObject * Enemy::Locate(string id)
{
	if (AreYou(id)) {
		return this;
	}
	if (_isAlive) {
		return nullptr;
	}
	GameObject* result;

	result = _inventory->Fetch(id);

	return result;
}


Enemy::~Enemy()
{
}
