#pragma once
#include "GameObject.h"
#include "Inventory.h"
#include "IHaveInventory.h"
#include "Location.h"
#include "AttackComponent.h"
#include "HealthComponent.h"

using namespace std;

class Player : public GameObject, public IHaveInventory
{
private:
	Inventory* _inventory;
	Location* _location;
	AttackComponent* _attack;
	HealthComponent* _health;
public:
	Player();
	Player(string name, string desc);

	string FullDescription() const;
	string Name() const;

	Inventory* GetInventory();
	GameObject* Locate(string id);
	AttackComponent* GetAttackComponent();
	HealthComponent* GetHealthComponent();

	void SetLocation(Location* location);
	Location* GetLocation();

	~Player();
};

