#include "stdafx.h"
#include "HealthComponent.h"


HealthComponent::HealthComponent()
{
}

HealthComponent::HealthComponent(int maxHP):HealthComponent(maxHP, maxHP)
{
}

HealthComponent::HealthComponent(int currentHP, int maxHP)
{
	_maxHP = maxHP;
	_currentHP = currentHP;
}

string HealthComponent::GetHealth()
{
	return "HP: " + to_string(_currentHP) + "/" + to_string(_maxHP);
}

void HealthComponent::ReduceHealth(int damage)
{
	_currentHP -= damage;
	if (_currentHP < 0) {
		_currentHP = 0;
	}
}

HealthComponent::~HealthComponent()
{
}
