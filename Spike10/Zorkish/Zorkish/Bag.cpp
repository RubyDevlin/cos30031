#include "stdafx.h"
#include "Bag.h"


Bag::Bag()
{
}

Bag::Bag(vector<string> ids, string name, string desc, bool open) : Item(ids, name, desc)
{
	_open = open;
	_inventory = new Inventory();
	_IHaveInventory = true;
}

string Bag::FullDescription()
{
	
	if (_open) {
		return "In the " + Name() + " you can see" + FullDescription();
	}
	else {
		return "The " + Name() + " is closed";
	}	
}

bool Bag::IsOpen() 
{
	return _open;
}

string Bag::Name() const
{
	return GameObject::Name();
}

string Bag::Close()
{
	if (_open) {
		_open = false;
		return "The " + Name() + " has been closed";
	} else {
		return "The " + Name() + " is already closed";
	}
}

string Bag::Open()
{
	if (!_open) {
		_open = true;
		return "The " + Name() + " is now open";
	}
	else {
		return "The " + Name() + " is already open";
	}
}

Inventory* Bag::GetInventory()
{
	return _inventory;
}

GameObject * Bag::Locate(string id)
{
	if (AreYou(id)) {
		return this;
	}
	else if (_open) {
		return _inventory->Fetch(id);
	} else {
		return nullptr;
	}
}



Bag::~Bag()
{
}
