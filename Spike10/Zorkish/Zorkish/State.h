#pragma once
#include <iostream>
#include "GameController.h"
#include <vector>
using namespace std;

class State
{
public:
	State();
	virtual ~State();
	virtual void Output() = 0;
	virtual void Input(GameController* g, vector<string> s) = 0;
};

