#pragma once
#include "State.h"
class Gameplay :
	public State
{
public:
	Gameplay();
	~Gameplay();
	void Output();
	void Input(GameController* g, string s);
};

