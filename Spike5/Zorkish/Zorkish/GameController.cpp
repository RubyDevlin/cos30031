#include "stdafx.h"
#include "GameController.h"
#include "State.h"
#include "MainMenu.h"
#include <iostream>
#include <string>
using namespace std;


GameController::GameController()
{
	_state = new MainMenu();
	quit = false;
}

GameController::~GameController()
{
	delete _state;
}

void GameController::ChangeState(State * s)
{
	_state = s;
}

void GameController::Output()
{
	_state->Output();
}

void GameController::GameLoop()
{
	while (!quit) 
	{
		string userInput;
		Output();
		cin >> userInput;
		if (userInput == "quit")
		{
			Quit();
		} else {
			_state->Input(this, userInput);
		}
	}
}

void GameController::Quit()
{
	quit = true;
}
