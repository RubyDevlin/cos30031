#include "stdafx.h"
#include "Gameplay.h"
#include "MainMenu.h"
#include "ViewHallOfFame.h"


Gameplay::Gameplay()
{
}


Gameplay::~Gameplay()
{
}

void Gameplay::Output()
{
	cout << "Welcome to Zorkish:Void World This world is simple and pointless.Used it to test Zorkish phase 1 spec." << endl;
}

void Gameplay::Input(GameController * g, string s)
{
	if (s == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		g->ChangeState(new MainMenu());
		delete this;
	} else if (s == "hiscore") {
		g->ChangeState(new ViewHallOfFame());
		delete this;
	}
}
