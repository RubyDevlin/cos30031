#pragma once
#include "State.h"
class MainMenu :
	public State
{
public:
	MainMenu();
	~MainMenu();
	void Output();
	void Input(GameController* g, string s);
};

