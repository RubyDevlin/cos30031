// GridWorld.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "fstream"
#include "string"

using namespace std;
int x, y;
bool gameOver = false;

string grid[9][9] =
{
	{ "#","#","#","#","#","#","#","#","#" },
	{ "#","#","G"," ","D","#","D"," ","#" },
	{ "#","#"," "," "," ","#"," "," ","#" },
	{ "#","#","#","#"," ","#"," ","D","#" },
	{ "#","#"," "," "," ","#"," "," ","#" },
	{ "#","#"," ","#","#","#","#"," ","#" },
	{ "#","#"," "," "," "," "," "," ","#" },
	{ "#","#","#","S","#","#","#","#","#" },
	{ "#","#","#","#","#","#","#","#","#" }
};

void Input()
{
	string pInput;
	cin >> pInput;

	if (pInput == "n" || pInput == "N")
	{
		if (grid[x-1][y] == "#")
		{
			cout << "There's a wall there, try again";
		} 
		else if (grid[x-1][y] == "D")
		{
			cout << "You fell in a pit and died. Good job.";
			gameOver = true;
		}
		else if (grid[x - 1][y] == "G")
		{
			cout << "You found the gold. You win.";
			gameOver = true;
		}
		else {
			x--;
		}
	}
	if (pInput == "e" || pInput == "E")
	{
		if (grid[x][y+1] == "#")
		{
			cout << "There's a wall there, try again";
		}
		else if (grid[x][y+1] == "D")
		{
			cout << "You fell in a pit and died. Good job.";
			gameOver = true;
		}
		else if (grid[x][y+1] == "G")
		{
			cout << "You found the gold. You win.";
			gameOver = true;
		}
		else {
			y++;
		}
	}
	if (pInput == "s" || pInput == "S")
	{
		if (grid[x+1][y] == "#")
		{
			cout << "There's a wall there, try again";
		}
		else if (grid[x+1][y] == "D")
		{
			cout << "You fell in a pit and died. Good job.";
			gameOver = true;
		}
		else if (grid[x+1][y] == "G")
		{
			cout << "You found the gold. You win.";
			gameOver = true;
		}
		else {
			x++;
		}
	}
	if (pInput == "w" || pInput == "W")
	{
		if (grid[x][y-1] == "#")
		{
			cout << "There's a wall there, try again";
		}
		else if (grid[x][y-1] == "D")
		{
			cout << "You fell in a pit and died. Good job.";
			gameOver = true;
		}
		else if (grid[x][y-1] == "G")
		{
			cout << "You found the gold. You win.";
			gameOver = true;
		}
		else {
			y--;
		}
	}
	if (pInput == "q" || pInput == "Q")
	{
		gameOver = true;
	}
	cout << endl;
}

void Update()
{
	string movementOptions;
	movementOptions = "You can move:";
	if (grid[x - 1][y] == "#")
	{

	}
	else {
		movementOptions += " North (N) ";
	}
	if (grid[x][y + 1] == "#")
	{

	}
	else {
		movementOptions += " East (E) ";
	}
	if (grid[x + 1][y] == "#")
	{

	}
	else {
		movementOptions += " South (S) ";
	}
	if (grid[x][y-1] == "#")
	{

	}
	else {
		movementOptions += " West (W) ";
	}

	cout << movementOptions;
	cout << endl;
}

void Render()
{
	for (int i = 0; i < 9; i++)
	{
		string output;
		cout << endl;

		for (int j = 0; j < 9; j++)
		{
			if (i == x & j == y)
			{
				output += "P";
			} else {
				output += grid[i][j];
			}
		}
		cout << output;
	}
	cout << endl;
}

int main()
{
	x = 7;
	y = 3;
	cout << "Welcome to GridWorld!";
	cout << endl;

	while (gameOver == false)
	{ 
		Update();
		Render();
		Input();
	}

	system("pause");
	return 0;
}




