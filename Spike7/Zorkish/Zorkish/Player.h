#pragma once
#include "GameObject.h"
#include "Inventory.h"
#include "IHaveInventory.h"
#include "Location.h"

using namespace std;

class Player : public GameObject, public IHaveInventory
{
private:
	Inventory* _inventory;
	Location* _location;
public:
	Player();
	Player(string name, string desc);

	string FullDescription() const;
	string Name() const;

	Inventory* GetInventory();
	GameObject* Locate(string id);

	void SetLocation(Location* location);
	Location* GetLocation();

	~Player();
};

