#include "stdafx.h"
#include "Gameplay.h"
#include "MainMenu.h"
#include "ViewHallOfFame.h"
#include "Player.h"
#include <fstream>



Gameplay::Gameplay()
{
	_setupGame = false;
	Player _currentPlayer = Player();
	//_currentPlayer.SetLocation(_testLocation);
}


Gameplay::~Gameplay()
{
}

void Gameplay::SetupGame()
{
	GameWorld MountainWorld = GameWorld();
	string line;
	int _creatingLocation = 0;
	string _locationId, _locationName, _locationDescription;
	int _creatingPath = 0;
	string _pathDirection, _pathDescription, _pathStartingLocation;
	Location* _pathToId = new Location(vector<string>{"default"}, "default", "default");
	Location* _pathFromId  = new Location(vector<string>{"default2"}, "default2", "default2");
	//Location* _pathLocation = new Location();

	ifstream AdventureFile("MountainWorld.txt");
	if (AdventureFile.is_open()) {
		while (getline(AdventureFile, line)) {
			if (_creatingLocation == 3) {
				_locationDescription = line;
				_creatingLocation = 0;
				//cout << "yo!";
				MountainWorld.AddLocation(new Location(vector<string>{_locationId}, _locationName, _locationDescription));
				//cout << "created location";
			}
			if (_creatingLocation == 2) {
				_locationName = line;
				_creatingLocation++;
				//cout << "case";
			}
			if (_creatingLocation == 1) {
				_locationId = line;
				_creatingLocation++;
				//cout << "case";
			}
			if (line == "Location") {
				//cout << "case";
				_creatingLocation++;
			}

			if (_creatingPath == 4)
			{
				_pathDescription = line;
				_creatingPath = 0;
				Path* path = new Path(_pathDirection, _pathToId, _pathDescription);

				for each (Location* l in MountainWorld._locations)
				{
					//cout << "path4";
					//cout << l->FirstId() << " and " << _pathStartingLocation << endl;
					if (l->FirstId() == _pathStartingLocation) {
						//cout << "Hello Ruby, you're the best" << endl;
						l->AddPath(path);
						//cout << l->PathList() << endl;
						
						//cout << "path4";
					}
				}
				
			}
			if (_creatingPath == 3) {
				for each (Location* l in MountainWorld._locations) {
					if (l->FirstId() == line) {
						_pathToId = l;
						//cout << "path3";
					}
				}
				_creatingPath++;
				
			}

			if (_creatingPath == 2) {
				_pathDirection = line;
				_creatingPath++;
				//cout << "path2";
			}

			if (_creatingPath == 1) {
				_pathStartingLocation = line;
				for each (Location* l in MountainWorld._locations) {
					if (l->FirstId() == line) {
						_pathFromId = l;
					}
				}
				//cout << "path1";
				_creatingPath++;
			}
			if (line == "ConnectionPath") {
				_creatingPath++;
				//cout << "path";
			}

			//cout << line << endl;
			//system("pause");
			
			_setupGame = true;
		}
	}
	//cout << to_string(MountainWorld._locations.size()) << endl;
	_currentPlayer.SetLocation(MountainWorld._locations[0]);
	//for each(Location* l in MountainWorld._locations) {
		//cout << l->Name() << endl;
		//cout << l->PathList() << endl;
	//}
}
void Gameplay::Output()
{
	if (_setupGame == false) {
		SetupGame();
		cout << "Welcome to Zorkish:Mountain World. This world is simple and pointless, like all text adventure worlds." << endl;
	}
	cout << _currentPlayer.GetLocation()->FullDescription() << endl;
}

void Gameplay::Input(GameController * g, string s)
{
	if (s == "quit")
	{
		cout << "Your adventure has ended without fame or fortune." << endl;
		g->ChangeState(new MainMenu());
		delete this;
	} else if (s == "hiscore") {
		g->ChangeState(new ViewHallOfFame());
		delete this;
	} else if (s == "go") {
		cout << "Where would you like to go?" << endl;
		cin >> s;
		MovementInput(s);
	}
}

void Gameplay::MovementInput(string s)
{
	if (s == "north")
	{
		for each (Path* p in _currentPlayer.GetLocation()->_paths) {
			if (p->FirstId() == "north") {
				_currentPlayer.SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	if (s == "east")
	{
		for each (Path* p in _currentPlayer.GetLocation()->_paths) {
			if (p->FirstId() == "east") {
				_currentPlayer.SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	if (s == "south")
	{
		for each (Path* p in _currentPlayer.GetLocation()->_paths) {
			if (p->FirstId() == "south") {
				_currentPlayer.SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
	if (s == "west")
	{
		for each (Path* p in _currentPlayer.GetLocation()->_paths) {
			if (p->FirstId() == "west") {
				_currentPlayer.SetLocation(p->ConnectedLocation());
				cout << p->DirectionDescription() << endl;
			}
		}
	}
}
