#pragma once
#include "IdentifiableObject.h"

using namespace std;

class GameObject : public IdentifiableObject
{
private:
	string _name;
	string _description;
public:
	GameObject();
	GameObject(vector<string> &arr, string name, string desc);

	virtual string FullDescription() const;
	virtual string Name() const;
	string ShortDescription() const;

	~GameObject();
};

