#pragma once
#include "GameObject.h"
#include "Path.h"
class Location :
	public GameObject
{
private:
	
public:
	Location();
	Location(vector<string> &ids, string name, string desc);
	vector<Path*> _paths;
	string FullDescription() const;
	string PathList() const;
	string Name() const;
	void AddPath(Path* p);
	~Location();
};

