#pragma once
#include "State.h"
#include "Player.h"
#include "Location.h"
#include "GameWorld.h"

class Gameplay :
	public State
{
private:
	bool _setupGame;
	GameWorld MountainWorld;
public:
	Location* _testLocation;
	Player _currentPlayer;
	Gameplay();
	~Gameplay();
	void SetupGame();
	void Output();
	void Input(GameController* g, string s);
	void MovementInput(string s);
};

