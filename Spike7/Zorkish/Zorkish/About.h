#pragma once
#include "State.h"
class About :
	public State
{
public:
	About();
	~About();
	void Output();
	void Input(GameController* g, string s);
};

