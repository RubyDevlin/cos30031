#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

class IdentifiableObject
{
private:
	vector<string> _identifier;
public:
	IdentifiableObject();
	IdentifiableObject(vector<string> &arr);

	bool AreYou(string id) const;
	string FirstId() const;

	void AddIdentifier(string id);

	~IdentifiableObject();
};
