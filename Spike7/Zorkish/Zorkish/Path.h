#pragma once
#include "GameObject.h"

using namespace std;
class Location;

class Path : public GameObject
{
private:
	Location* _connectedLocation;
	string _directionDescription;
	string _direction;
public:
	Path();
	Path(string direction, Location* connectedlocation, string desc);

	string DirectionDescription();
	Location* ConnectedLocation();

	~Path();
};

