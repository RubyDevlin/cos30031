#pragma once
#include <string>
#include "Inventory.h"

using namespace std;

class GameObject;

class IHaveInventory
{
public:
	~IHaveInventory() {}

	virtual string Name() const = 0;

	virtual Inventory* GetInventory() = 0;
	virtual GameObject* Locate(string id) = 0;
};
