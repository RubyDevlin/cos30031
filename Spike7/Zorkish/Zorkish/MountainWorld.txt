Location
mountainbase
Base of Mountain
You are standing at the base of a rather large mountain.

Location
forest
Thick Forest
You are engulfed by trees.

Location
mountainface
Face of Mountain
You are standing at the face of the mountain.

Location
mountaintop
Top of Mountain
You are standing on top of the mountain, paralised with fear. You cannot move.

ConnectionPath
mountainbase
East
forest
You move east, towards a thick forest.

ConnectionPath
mountainbase
North
mountainface
You approach the face of the mountain.

ConnectionPath
forest
West
mountainbase
You head west to the base of the mountain.

ConnectionPath
mountainface
South
mountainbase
You travel back to the base of the mountain.

ConnectionPath
mountainface
North
mountaintop
You start to scale the mountain.

Item
fork
Sharp Fork
A sharp piece of cutlery.
Player

Item
knife
Butter Knife
A blunt knife for spreading butter.
mountainbase

End.