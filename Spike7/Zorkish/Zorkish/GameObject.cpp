#include "stdafx.h"
#include "GameObject.h"

GameObject::GameObject()
{
}

GameObject::GameObject(vector<string> &ids, string name, string desc)
	: IdentifiableObject(ids)
{
	_name = name;
	_description = desc;
}

string GameObject::FullDescription() const
{
	return _description;
}

string GameObject::Name() const
{
	return _name;
}

string GameObject::ShortDescription() const
{
	return _name + " (" + FirstId() + ")";
}

GameObject::~GameObject()
{
}
