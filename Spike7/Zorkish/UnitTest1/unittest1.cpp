#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Zorkish/GameWorld.h"
#include "../Zorkish/Location.h"
#include "../Zorkish/Path.h"
#include "../Zorkish/Gameplay.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestLocationAddedToWorld)
		{
			GameWorld* testWorld = new GameWorld();
			Location* testLocation = new Location(vector<string>{"testLocation"}, "Test Location", "A location for testing");
			testWorld->AddLocation(testLocation);
			Assert::IsTrue(testWorld->_locations[0] == testLocation);
		}

	};

	TEST_CLASS(UnitTest2)
	{
		TEST_METHOD(TestPathAddedToLocation)
		{
			Location* testLocation1 = new Location(vector<string>{"testLocation1"}, "Test Location 1", "The first location for testing");
			Location* testLocation2 = new Location(vector<string>{"testLocation2"}, "Test Location 2", "The second location for testing");
			Path* testPath = new Path("north", testLocation2, "A path leading north");
			testLocation1->AddPath(testPath);
			Assert::IsTrue(testLocation1->_paths[0] == testPath);

		}
	};
}